import { Selector } from 'testcafe';

const submitButton = Selector('#submitButton');
const userName = Selector('#userName');
const sucsessMessage = Selector('#sucsessMessage');

// Test
fixture `Test inputName field:`
    .page `E:/OSPanel/domains/QAtest/checklist/index.html`;

test(`Name input has more than 2 characters`, async t => {
    await t
        .typeText(userName, 'd')
        .click(submitButton())
        .expect(sucsessMessage.count).eql(0)
});
test(`Name input has onley letters`, async t => {
    await t
        .typeText(userName, 'User1')
        .click(submitButton())
        .expect(sucsessMessage.count).eql(0)
});
test(`Valid name`, async t => {
    await t
        .typeText(userName, 'Viktor')
        .click(submitButton())
        .expect(sucsessMessage.count).eql(1)
});