# Опис виконанного завдання

**1. Fork-ни проект, та поклади всі завдання в папку checklist.**

    Done

**2. Завдання на написання тест кейсів:**

    1.1. На перевірку критичного функціоналу.

      Користувач повинен ввести у поля з зірочкою данні.**blocker**
      Користувая повинен дати згоду на обробку данних.**blocker**
      Користувачвач повинен успішно пройти CAPTCHA тест.**blocker**
      Користувачвач повинен натиснути кнопку "зарееструватись".**blocker**

    1.2. На перевірку важливого функціоналу.

      Користувач повинен ввести ввести у поле імені > 2-х літер вказаною мовою.**critical**
      Користувач повинен використоувати пароль у якому є числа, великі та малі літери.**major**
      Користувач може завантажити замість фото - файл з скриптом, або зображення завеликого розміру.**critical**

    1.3. На перевірку не пріоритетного функціоналу.

      Користувач може вибрати відображення родинного дерева після реестрації.**minor**
      Користувач може додати "додаткове поле" з данними родича.**minor**
      Користувач може вибрати які саме данні будуть відображені у родинному дереві.**minor**

**3. В формі є поле "Номер телефону", яке приймає тільки цифри. Яким чином можна
обійти це правило, й ввести в такий інпут текст та відправити його на сервер, маючи доступ до продукта тільки
в браузері?**

    Для того, щоб поле input приймало "тільки цифри" використовують декілька варыантів реалізації, а саме: 

    - стандартний атрибут type="number"
    - HTML5 атрибут pattern="^[0-9]+$"
    - js валідація

    У випадку атрибуту type та pattern: 

    1.1 Відкрити ChromDevTools у вкладці Elements. 
    1.2 Знайти по DOM дереву необхідний input.
    1.3 Відредагувати атрибут type змінивши його значення на "text", або якщо це паттерн то редагувати його.

    У випадку з js валідаціею поля, можна вимкнути виконання JavaScript на сторінці і відправити будьяку форму через звичайний submit.

    2.1 Відкрити ChromDevTools у вкладці Elements.
    2.2 Customize and control DevTools-=>Settings-=>Preferences-=>Debugger-=>Disable JavaScript

    У випадку з js валідаціею та сторонными программами - можна відправити пакет через proxy, на якому перехопити цей пакет та відредагувати данні.

**4. Створіть check-list для перевірки валідації поля E-mail. Check-list має
складатися із значень, які Ви будете вносити до інпуту.**

    invalid emails:
    * myname
    * myname@.com.my
    * myname123@gmail.a
    * myname123@.com
    * myname123@.com.com
    * .myname@myname.com
    * myname()*@gmail.com
    * myname@%*.com
    * myname..2002@gmail.com
    * myname.@gmail.com
    * myname@myname@gmail.com
    * myname@gmail.com.1a

    valid emails:
    * myname@yahoo.com
    * myname-100@yahoo.com
    * myname.100@yahoo.com
    * myname111@myname.com
    * myname-100@myname.net
    * myname.100@myname.com.au
    * myname@1.com
    * myname@gmail.com.com
    * myname+100@gmail.com
    * myname-100@yahoo-test.com


## Бонус пойнти

**1. Проставити severity для кожного правила із чекліста для Email-а (blocker, critical, major, minor).**

    Прыоріорітети дописанно жирним у другому завданні.

**2. Напишіть автотест на перевірку будь-якого правила, або тест-кейса. Забороняється використання screen recording.**

    - створимо тестовий екземпляр сторінки з полем вводу імені ../checklist/index.html
    - створимо ../checklist/input-name-test.js
    - перейдемо у теку з тестовим файлом та запустимо тест коммандою:
        testcafe chrome input-name-test.js
    - Як бачимо, всі три перевірки тесту виконалися успішно!
        Test inputName field:
          √ Name input has more than 2 characters
          √ Name input has onley letters
          √ Valid name

**3. В залежності від того, Ви знайомі з SQL чи GraphQL, зробіть одне або 2 завдання:**
   * Напишіть GraphQl запит на отримання всіх даних про користувачів системи, маючи такі дані з GraphQl Schema:
     ```graphql
     type Query {
       users(pagination: PaginationValue!): UserList!
     }

     type UserList {
       items: [User!]!
     }

     type User {
       id: ID!
       name: String!
       username: String!
       password: String!
     }
     ```

* Напишіть SQL запит на отримання всіх користувачів, чий e-mail зареєстрований на `gmail.com`, маючи такі SQL таблиці:
     ```
     Перша таблиця: Users (id, name, email)
     Друга таблиця: UserRoles (user_id, role_id)
     ```
**Відповідь**
SELECT name FROM `Users` WHERE email like '%@gmail.com%'